package com.apptivitylab.learn.journaling.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.model.Journal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Admin on 26/09/2016.
 */

public class HeaderViewHolder extends RecyclerView.ViewHolder {

    private TextView mHeaderTextView;

    public HeaderViewHolder(View itemView) {
        super(itemView);

        mHeaderTextView = (TextView) itemView.findViewById(R.id.cell_header_tv_month_header);
    }

    public void setHeaderTitle(Journal journal) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, journal.getMonth());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
        mHeaderTextView.setText(simpleDateFormat.format(calendar.getTime()));
    }
}

package com.apptivitylab.learn.journaling.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.apptivitylab.learn.journaling.R;

/**
 * Created by aarief on 05/10/2016.
 */

public class WalkthroughActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        Button finishWalkthrough = (Button)findViewById(R.id.activity_walkthrough_button_finish);

        finishWalkthrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}

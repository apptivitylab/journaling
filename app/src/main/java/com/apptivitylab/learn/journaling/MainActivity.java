package com.apptivitylab.learn.journaling;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.apptivitylab.learn.journaling.ui.JournalListFragment;
import com.apptivitylab.learn.journaling.ui.WalkthroughActivity;

public class MainActivity extends AppCompatActivity {
    private SharedPreferences mSharedPreferences;
    private int mLaunchCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSharedPreferences = getSharedPreferences(getString(R.string.pref_file_key), MODE_PRIVATE);
        mLaunchCount = mSharedPreferences.getInt("LAUNCH_COUNT", 0);

        if (mLaunchCount == 0) {
            Intent walkthroughIntent = new Intent(this, WalkthroughActivity.class);
            startActivityForResult(walkthroughIntent, 0);
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_main_container, new JournalListFragment())
                    .commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mLaunchCount++;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt("LAUNCH_COUNT", mLaunchCount);
        editor.apply();
    }
}

package com.apptivitylab.learn.journaling.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.api.RestAPIClient;
import com.apptivitylab.learn.journaling.model.Journal;

import java.util.Calendar;

/**
 * Created by aarief on 03/10/2016.
 */

public class AddEntryFragment extends Fragment {
    private EditText mEntryTitleEditText;
    private EditText mEntryDetailsTextView;
    private Button mAddButton;
    private Button mCancelButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_entry, container, false);

        mEntryTitleEditText = (EditText) rootView.findViewById(R.id.fragment_add_entry_et_title);
        mEntryDetailsTextView = (EditText) rootView.findViewById(R.id.fragment_add_entry_tv_detail);
        mAddButton = (Button) rootView.findViewById(R.id.fragment_add_entry_btn_add);
        mCancelButton = (Button) rootView.findViewById(R.id.fragment_add_entry_btn_cancel);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entryTitle = mEntryTitleEditText.getText().toString();
                String entryDetails = mEntryDetailsTextView.getText().toString();

                Journal journal = new Journal(entryTitle, entryDetails, Calendar.getInstance());

                RestAPIClient.getInstance(getContext()).addJournal(journal, new RestAPIClient.OnAddJournalCompletedListener() {
                    @Override
                    public void onComplete(String objectId, VolleyError error) {
                        if (error != null) {
                            Snackbar.make(getView(), error.toString(), Snackbar.LENGTH_LONG).show();
                        } else {
                            Intent resultIntent = new Intent();
                            resultIntent.putExtra(JournalListFragment.EXTRA_JOURNAL_ID, objectId);

                            getActivity().setResult(Activity.RESULT_OK, resultIntent);
                            getActivity().finish();
                        }
                    }
                });
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        });
    }
}

package com.apptivitylab.learn.journaling.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.controller.UserController;
import com.apptivitylab.learn.journaling.model.Journal;
import com.apptivitylab.learn.journaling.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RestAPIClient
 * Journaling-Android
 * <p>
 * Created by Li Theen Kok on 30/09/2016/09/2016.
 * Copyright (c) 2016 Apptivity Lab. All Rights Reserved.
 */

public class RestAPIClient {

    private static RestAPIClient sSharedInstance;

    private RequestQueue mRequestQueue;
    private String mUserToken;
    private SharedPreferences mSharedPreferences;

    // Private constructor for Singleton pattern
    private RestAPIClient(@NonNull Context context) {
        // We'll use this RequestQueue to dispatch our requests
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());

        mSharedPreferences = context.getSharedPreferences(context.getString(R.string.pref_file_key), Context.MODE_PRIVATE);
        mUserToken = loadUserToken();
    }

    // Get singleton instance using this static method
    public static synchronized RestAPIClient getInstance(@NonNull Context context) {
        if (sSharedInstance == null) {
            sSharedInstance = new RestAPIClient(context);
        }

        return sSharedInstance;
    }

    private Request newRequest(int method, String url, JSONObject parameters, Response.Listener successListener, Response.ErrorListener errorListener) {
        BackendlessJsonObjectRequest request = new BackendlessJsonObjectRequest(method, url, parameters, successListener, errorListener);

        if (mUserToken != null) {
            request.putHeader("user-token", mUserToken);
        }

        return request;
    }

    public interface OnGetJournalsCompletedListener {
        void onComplete(List<Journal> journals, VolleyError error);
    }

    public void getJournals(final OnGetJournalsCompletedListener completionListener) {
        String url = "https://api.backendless.com/v1/data/Journals";

        Request request = newRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<Journal> journals = new ArrayList<>();

                JSONArray dataArray = response.optJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataJson = dataArray.optJSONObject(i);
                    Journal journal = null;
                    try {
                        journal = new Journal(dataJson);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    journals.add(journal);
                }
                completionListener.onComplete(journals, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public interface OnAddJournalCompletedListener {
        void onComplete(String objectId, VolleyError error);
    }

    public void addJournal(Journal journal, final OnAddJournalCompletedListener completionListener) {
        String url = "https://api.backendless.com/v1/data/Journals";

        Request request = newRequest(Request.Method.POST, url, journal.toJsonObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String objectId = response.optString("objectId");

                completionListener.onComplete(objectId, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public interface OnUserAuthenticationCompleteListener {
        void onComplete(User user, VolleyError error);
    }

    public void signUp(String email, String password, final OnUserAuthenticationCompleteListener completionListener) {
        String url = "https://api.backendless.com/v1/users/register";

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("email", email);
            parameters.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                User user = new User(response);
                saveUserToken(user.getUserToken());
                UserController.getInstance().setLoggedInUser(user);

                completionListener.onComplete(user, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public void login(String email, String password, final OnUserAuthenticationCompleteListener completionListener) {
        String url = "https://api.backendless.com/v1/users/login";

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("login", email);
            parameters.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                User user = new User(response);
                saveUserToken(user.getUserToken());
                UserController.getInstance().setLoggedInUser(user);

                completionListener.onComplete(user, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public interface OnUploadFileCompleteListener {
        void onUploadComplete(String path, VolleyError error);
    }

    public void uploadFile(File file, String mimeType, final OnUploadFileCompleteListener listener) {

        String path = "http://api.backendless.com/v1/files/media/" + file.getName() + "?overwrite=true";

        BackendlessUploadRequest uploadRequest = new BackendlessUploadRequest(
                Request.Method.POST,
                path,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String path = response.optString("fileURL");
                        if (listener != null) {
                            listener.onUploadComplete(path, null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener != null) {
                            listener.onUploadComplete(null, error);
                        }
                    }
                }
        );

        uploadRequest.setFile(file, "upload", mimeType);
        mRequestQueue.add(uploadRequest);
    }


    private void saveUserToken(String token) {
        mUserToken = token;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("USER_TOKEN", token);
        editor.apply();
    }

    private String loadUserToken() {
        String token = mSharedPreferences.getString("USER_TOKEN", null);
        return token;
    }
}

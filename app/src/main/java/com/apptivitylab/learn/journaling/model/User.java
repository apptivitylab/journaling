package com.apptivitylab.learn.journaling.model;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * User
 * Journaling-Android
 * <p>
 * Created by Li Theen Kok on 30/09/2016/09/2016.
 * Copyright (c) 2016 Apptivity Lab. All Rights Reserved.
 */

public class User {

    private String mId;
    private String mEmail;
    private String mName;
    private String mUserToken;

    public User(@NonNull JSONObject jsonObject) {
        /*
        {
        	"userStatus": "ENABLED",
	        "created": 1475211805000,
	        "name": null,
	        "ownerId": "CC06DDDF-4E42-3B3A-FF73-55EA7886D900",
	        "updated": null,
	        "email": "demo1@apptivitylab.com",
	        "objectId": "CC06DDDF-4E42-3B3A-FF73-55EA7886D900",
	        "__meta": "{\"relationRemovalIds\":{},\"selectedProperties\":[\"password\",\"created\",\"___saved\",\"___class\",\"name\",\"ownerId\",\"updated\",\"email\",\"objectId\"],\"relatedObjects\":{}}"
        }
         */

        mId = jsonObject.optString("ownerId");
        mEmail = jsonObject.optString("email");
        mName = jsonObject.optString("name");
        mUserToken = jsonObject.optString("user-token");
    }

    public String getId() {
        return mId;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getName() {
        return mName;
    }

    public String getUserToken() {
        return mUserToken;
    }
}

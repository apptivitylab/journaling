package com.apptivitylab.learn.journaling.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aarief on 23/09/2016.
 */

public class Notebook implements Parcelable {
    private String mTitle;
    private String mDescription;
    private ArrayList<String> mTagList;
    private ArrayList<Journal> mJournalList;

    //region Getters and Setters
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public List<String> getTagList() {
        return mTagList;
    }

    public void setTagList(ArrayList<String> tagList) {
        mTagList = tagList;
    }

    public List<Journal> getJournalList() {
        return mJournalList;
    }

    public void setJournalList(ArrayList<Journal> journalList) {
        mJournalList = journalList;
    }

    //endregion

    //region Parcelable
    protected Notebook(Parcel in) {
        mTitle = in.readString();
        mDescription = in.readString();
        mTagList = in.createStringArrayList();
        mJournalList = in.createTypedArrayList(Journal.CREATOR);
    }

    public static final Creator<Notebook> CREATOR = new Creator<Notebook>() {
        @Override
        public Notebook createFromParcel(Parcel in) {
            return new Notebook(in);
        }

        @Override
        public Notebook[] newArray(int size) {
            return new Notebook[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mTitle);
        parcel.writeString(mDescription);
        parcel.writeStringList(mTagList);
        parcel.writeTypedList(mJournalList);
    }
    //endregion
}

package com.apptivitylab.learn.journaling;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.apptivitylab.learn.journaling.controller.JournalLoader;
import com.apptivitylab.learn.journaling.model.Journal;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private Context mContext;

    @Before
    public void setupContext() {
        mContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.apptivitylab.learn.journaling", appContext.getPackageName());
    }

    @Test
    public void appNameInResourceShouldMatch() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        String appTitle = appContext.getResources().getString(R.string.app_name);
        assertEquals(appTitle, "Journaling");
    }

    @Test
    public void saveFile() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        List<Journal> journalList = JournalLoader.loadSamples(appContext);
        JSONArray jsonArray = new JSONArray();
        for (Journal journal : journalList) {
            jsonArray.put(journal.toJsonObject());
        }

        File outputFile = new File(appContext.getFilesDir(), "journalsList.json");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            fileOutputStream.write(jsonArray.toString().getBytes());

            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void readFile() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        File inputFile = new File(appContext.getFilesDir(), "journalsList.json");
        try {
            FileInputStream inputStream = new FileInputStream(inputFile);

            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);

            String output = new String(buffer);

            inputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteFile() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        File inputFile = new File(appContext.getFilesDir(), "journalsList.json");

        if (inputFile.exists()) {
            inputFile.delete();
        }
    }
}

package com.apptivitylab.learn.journaling;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void stringComparisonShouldUseEquals() {
        String one = "Hello";
        String two = "Hello";
        String three = new String("Hello");

        // Correct way to compare strings is to use equals
        assertEquals(one, two);
        assertEquals(two, three);
    }

    @Test
    public void stringComparisonCannotUseOperators() {
        String one = "Hello";
        String two = "Hello";
        String three = new String("Hello");

        assertTrue(one == two);
        assertFalse(two == three);
    }


}